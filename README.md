# NYC Subway Schedules

A project to convert current and previous NYC subway schedule data going back to 2013

GTFS data from the MTA shows a better and more precise view of train schedules.

# Quick Start

Schedules are available [here](https://rydays-gtfs-nycsubway.bitbucket.io/). For now, just the 2020-01-09 schedules are available.

There are problems with the logic in the code when it comes to routes that are operating in multiple parts. For example, due to Hurricane Sandy damage, (R) trains ran in two parts and did not run through the Montague Street Tunnel in part of 2013. The schedule generation works off of a "common station" somewhere in the middle of a service's typical route pattern in order to determine how to fit a train in a table, since not all trains for a designated service originate at the same station. A service running in two sections doesn't have a common station, thus breaking the program.

Code will be released once the above issue is fixed.

# Details

The public schedules distributed by NYCT do not represent the full extent of service as readability is often chosen over accurate data, which, for researchers, precludes analysis of service frequency and other metrics. This is a list of condensing and omissions often found in the public schedules, along with examples from the actual public schedules:

- [(1) line](http://web.mta.info/nyct/service/pdf/t1cur.pdf) or any line: rush hour and mid-day periods are collapsed into an interval so the reader can't see the specific departure/arrival times. For rush hour trains, this is not a big deal as frequency is very high and departure/arrival times are variable and fragile. For off-peak times with frequencies greater than 5 minutes, this could be quite an annoyance.

- [(A) line](http://web.mta.info/nyct/service/pdf/tacur.pdf): The (A) line usually has 2 terminals/origins on the southern end and sometimes a 3rd during rush hours. In one spot, the public schedule states "every 15 minutes from either Far Rockaway or Lefferts Blvd" for a 3 hour period, making planning more difficult.

- [(5) line](http://web.mta.info/nyct/service/pdf/t5cur.pdf): Several rush hour trains originate from (or terminate at) Crown Heights-Utica Ave (normally served by the (3) and (4)) - but do these rush-hour (5) trains make express stops east of Franklin Ave like the (4) or local stops like the (3)?

- [(E) line](http://web.mta.info/nyct/service/pdf/tecur.pdf): Like the (5) with an alternate terminal, several (E) rush hour trains originate/terminate on the F line at Jamaica-179th Street due to terminal capacity constraints at Jamaica Center–Parsons/Archer. What you can't see from the schedule is that all but two of these (E) trains actually only make "express" stops along Hillside Avenue. (The Queens Boulevard Line is overbuilt in this section due to planned expansions that never came through) Only two northbound evening trains stop at Sutphin Boulevard and 169th Street: the (weekday) WTC 18:40 and the WTC 19:20+. Thus, you can get a one-seat ride in the evening to Sutphin Blvd or 169th Street with the (E) train, but not the other around.

- [(F) line](http://web.mta.info/nyct/service/pdf/tfcur.pdf): Four (F) trips actually run express in a historically unused express section in Brooklyn. Despite the service being a "pilot", these trips appear in GTFS data but not in public schedules.